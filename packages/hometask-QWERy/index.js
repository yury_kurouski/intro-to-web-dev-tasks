/* eslint-disable no-undef */

const navbar = document.querySelector('.header');
window.onscroll = () => {
  if (window.scrollY > 60 ) {
    navbar.classList.add('faded');
  } else  {
    navbar.classList.remove('faded');
  }
};