import Header from './components/header'
import Hero from './components/hero'
import Products from './components/products'
import LatestProduct from './components/latest-product'
import InTheBox from './components/in-the-box'
import Subscribe from './components/subscribe'
import Footer from './components/footer'
import Container from './components/container'

const app = document.querySelector('.app');

const render = () => {
  app.innerHTML = `
  ${ Container(Header(), Hero()) }
  ${ Products() }
  ${ LatestProduct() }
  ${ InTheBox() }
  ${ Subscribe() }
  ${ Footer() }
  `
}

export default render 
