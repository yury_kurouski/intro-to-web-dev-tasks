import ProductImg3 from '../../../assets/images/product-03.png';
import ProductImg2 from '../../../assets/images/product-02.png';
import ProductImg1 from '../../../assets/images/product-01.png';

const latestProduct = () => `
  <section class="latest-product container">
    <h2 class="latest_title">
      Our Latest Product
    </h2>
    <p class="latest_descr">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc ipsum aliquam, ante.
    </p>

    <div class="products-cards">
      <div class="product-card card-red inactive">
        <img src=${ ProductImg3 } alt="Product image" class="product_img">
        <div class="rating_container">
          <ul class="rating_stars">
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
          </ul>
          <span class="rating_number">4.50</span>
        </div>
        <div class="card_info">
          <span class="card_title">
            Red Headphone
          </span>
          <span class="card_price">$ 235</span>
        </div>
      </div>
      <div class="product-card">
        <img src=${ ProductImg2 } alt="Product image" class="product_img">
        <div class="rating_container">
          <ul class="rating_stars">
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
          </ul>
          <span class="rating_number">4.50</span>
        </div>
        <div class="card_info">
          <span class="card_title">
            Blue Headphone
          </span>
          <span class="card_price">$ 235</span>
        </div>
      </div>
      <div class="product-card card-green inactive">
        <img src=${ ProductImg1 } alt="Product image" class="product_img">
        <div class="rating_container">
          <ul class="rating_stars">
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
            <li class="rating_star fa fa-star checked"></li>
          </ul>
          <span class="rating_number">4.50</span>
        </div>
        <div class="card_info">
          <span class="card_title">
            Green Headphone
          </span>
          <span class="card_price">$ 235</span>
        </div>
      </div>
    </div>
  </section>
`

export default latestProduct