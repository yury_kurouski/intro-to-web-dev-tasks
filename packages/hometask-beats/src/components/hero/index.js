import HeroImg from '../../../assets/images/hero.png';

const hero = () => `
  <main class="hero">
    <img src=${HeroImg} alt="Headphones" class="hero-img">
    <div class="hero_info-container">
      <h2 class="hero_subtitle">
        Hear it. Feel it
      </h2>
      <h1 class="hero_title">Move with the music</h1>
      <div class="hero_price-box">
        <span class="price-box_actual-price">$ 435</span>
        <span class="price-box_vertical-divider">&#124;</span>
        <span class="price-box_old-price">$ 465</span>
      </div>

      <button class="hero_buy-btn main-button white-button">
        BUY NOW
      </button>
    </div>
  </main>
`

export default hero