import LogoImg from '../../../assets/icons/logo.svg';
import SearchImg from '../../../assets/icons/search.svg';
import BoxImg from '../../../assets/icons/box.svg';
import UserImg from '../../../assets/icons/user.svg';

const header = () => `
  <header>
    <nav class="navigation">
      <img src=${ LogoImg } alt="Logotype" class="logo">

      <ul class="navigation_links">
        <li class="navigation_link">
          <img src=${ SearchImg } alt="Search" class="navigation_link_img">
        </li>
        <span class="navigation_links_vertical-divider">&#124;</span>
        <li class="navigation_link">
          <img src=${ BoxImg } alt="Cart" class="navigation_link_img">
        </li>
        <span class="navigation_links_vertical-divider">&#124;</span>
        <li class="navigation_link">
          <img src=${ UserImg } alt="Search" class="navigation_link_img">
        </li>
      </ul>

      <div class="navigation_burger">
        <div class="burger-line wide-line"></div>
        <div class="burger-line narrow-line"></div>
        <div class="burger-line wide-line"></div>
        <div class="burger-line narrow-line"></div>
      </div>
    </nav>
  </header>
`

export default header;