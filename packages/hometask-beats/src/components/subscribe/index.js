const subscribe = () => `
<section class="subscribe">
  <h2 class="subscribe_title">
    Subscribe
  </h2>
  <p class="subscribe_text">
    Lorem ipsum dolor sit amet, consectetur
  </p>
  <div class="subscribe_input-container">
    <input type="text" class="subscribe_input" placeholder="Enter Your email address">
    <button class="subscribe_btn main-button red-button">
      Subscribe
    </button>
  </div>
</section>
`

export default subscribe