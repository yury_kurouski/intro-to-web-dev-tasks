const container = (...args) => `
    <div class="hero-container">
      ${ args.join('') }
    </div>
  `

export default container