import ArrowLeft from '../../../assets/icons/arrow-left.svg';
import ArrowRight from '../../../assets/icons/arrow-right.svg';
import Clour2 from '../../../assets/images/clour-02.png';
import Clour1 from '../../../assets/images/clour-01.png';
import Clour3 from '../../../assets/images/clour-03.png';
import BatteryImg from '../../../assets/icons/battery.svg';
import BluetoothImg from '../../../assets/icons/bluetooth.svg';
import MicImg from '../../../assets/icons/microphone.svg';
import FeatImg from '../../../assets/images/feature.png';


const products = () => `
  <section class="products-collection container">
    <h2 class="products-carousel_title">
      Our Latest
      <br>
      colour collection 2021
    </h2>
    <div class="products-carousel">
      <img src=${ ArrowLeft } alt="Left arrow" class="carousel_arrow arrow-left">
      <div class="carousel-card inactive">
        <img src=${ Clour2 } alt="" class="carousel-card_img">
      </div>
      <div class="carousel-card">
        <img src=${ Clour1 } alt="" class="carousel-card_img">
      </div>
      <div class="carousel-card inactive">
        <img src=${ Clour3 } alt="" class="carousel-card_img">
      </div>
      <img src=${ ArrowRight } alt="Right arrow" class="carousel_arrow arrow-right">
    </div>

    <!-- SPEC CONTAINER -->
    <div class="product-descr">
      <div>
        <h2 class="good-headphones_title">
          Good headphones and loud music is all you need
        </h2>

        <div class="specification_container">
          <div class="specification_icon_outer">
            <div class="specification_icon_inner">
              <img src=${ BatteryImg } alt="Battery" class="specification_icon_inner">
            </div>
          </div>
          <div class="specification_icon-fullfill fullfill_battery"></div>
          <div class="item-specification">
            <h3 class="specification_title">
              Battery
            </h3>
            <span class="specification_info">
              Battery 6.2V-AAC codec
            </span>
            <a href="#" class="specification_lern">Lern More</a>
          </div>
        </div>

        <div class="specification_container">
          <div class="specification_icon_outer">
            <div class="specification_icon_inner">
              <img src=${ BluetoothImg } alt="Battery" class="specification_icon_inner">
            </div>
          </div>
          <div class="specification_icon-fullfill fullfill_bluetooth"></div>
          <div class="item-specification">
            <h3 class="specification_title">
              Bluetooth
            </h3>
            <span class="specification_info">
              Battery 6.2V-AAC codec
            </span>
            <a href="#" class="specification_lern">Lern More</a>
          </div>
        </div>

        <div class="specification_container">
          <div class="specification_icon_outer">
            <div class="specification_icon_inner">
              <img src=${ MicImg } alt="Microfone" class="specification_icon_inner">
            </div>
          </div>
          <div class="specification_icon-fullfill fullfill_microphone"></div>
          <div class="item-specification">
            <h3 class="specification_title">
              Microphone
            </h3>
            <span class="specification_info">
              Battery 6.2V-AAC codec
            </span>
            <a href="#" class="specification_lern">Lern More</a>
          </div>
        </div>
      </div>
      <img src=${ FeatImg } alt="Feature photo" class="example-photo">
    </div>
  </section>
`

export default products