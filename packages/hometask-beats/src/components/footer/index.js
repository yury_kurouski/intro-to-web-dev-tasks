import LogoImg from '../../../assets/icons/logo.svg';
import InstagramLogo from '../../../assets/icons/instagram.svg';
import TwitterLogo from '../../../assets/icons/twitter.svg';
import FacebookLogo from '../../../assets/icons/facebook.svg';

const footer = () => `
  <footer class="footer">
    <img src=${ LogoImg } alt="Beats" class="footer_ling_img">
    <ul class="footer_navigation">
      <li class="nav-item">
        Home
      </li>
      <li class="nav-item">
        About
      </li>
      <li class="nav-item">
        Product
      </li>
    </ul>

    <ul class="footer_links">
      <li class="footer_link">
        <img src=${ InstagramLogo } alt="Instagram" class="footer_ling_img">
      </li>
      <li class="footer_link">
        <img src=${ TwitterLogo } alt="Twitter" class="footer_ling_img">
      </li>
      <li class="footer_link">
        <img src=${ FacebookLogo } alt="Facebook" class="footer_ling_img">
      </li>
    </ul>
  </footer>
`

export default footer