import BoxingImg from '../../../assets/images/boxing.png';

const inTheBox = () => `
  <section class="in-the-box container">
    <img src=${ BoxingImg } alt="Box image" class="box-img">
    <div>
      <h2 class="in-the-box_title">
        Whatever you get in the box
      </h2>
      <ul class="box-contains">
        <li class="box_item">
          5A Charger
        </li>
        <li class="box_item">
          Extra battery
        </li>
        <li class="box_item">
          Sophisticated bag
        </li>
        <li class="box_item">
          User manual guide
        </li>
      </ul>
    </div>
  </section>
`

export default inTheBox